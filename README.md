# About
Harpagon is (supposed to be) a modular and extensible budget manager. It is focused on defining terms and predicting spending in the next term (divided into categories)

# Architecture
## Save file structure
Save file is in JSON format for easy readability.
It includes object blueprints (e.g. transaction, payee/payer and term)
Each transaction is saved and assigned to a term.

# License
*I have NO idea*