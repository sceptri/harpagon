using Gtk

NothingOrGObject = Union{Nothing, GObject}

mutable struct Elementor
	objects::Dict{String, GObject}

	Elementor() = new(Dict{String, GObject}())
end

function getGlobalElementor()
	global elementor

	if !@isdefined elementor
		error("Global elementor object is not defined! Aborting...")
	end

	return elementor
end

getElementor() = getGlobalElementor()

function importFromBuilder!(elementor::Elementor, builder::Builder)
	if !isopen(builder)
		error("Cannot import closed Builder! Aborting...")
	end

	for object in builder.instance
		push!(elementor, object)
	end
end

function push!(elementor::Elementor, object::T) where T <: GObject
	object_name = get_gtk_property(object, :name, String)

	if object_name === ""
		error("Cannot add object with empty name to Elementor! Aborting...")
	end

	push!(elementor, object_name => object)
end

function push!(elementor::Elementor, object_pair::Pair{String, T}; overwrite = false, nameless = false) where T <: GObject
	object_key = first(object_pair)
	if object_key === ""
		error("Cannot add object with empty name to Elementor! Aborting...")
	end

	if object_key ∈ keys(elementor.objects)
		error("Cannot add object with the name $object_key twice! Aborting...")
	end

	# For nameless objects, do NOT care about their name
	if nameless
		push!(elementor.objects, object_pair)
		return
	end

	object = last(object_pair)
	object_name = get_gtk_property(object, :name, String)

	if object_key !== object_name && !overwrite
		error("Object names and keys must be the same in Elementor! Aborting...")
	elseif object_key !== object_name && overwrite
		DEBUG && println("Warning! Setting Elementor key $object_key as object name")
		set_gtk_property!(object, :name, object_key)
	end

	push!(elementor.objects, object_pair)
end

function getElement(element_name::AbstractString, elementor::Elementor = getElementor())
	return elementor.objects[string(element_name)]
end

#
# Wrapper functions around push! to GObjects, such that they are added to the Elementor as well
#

function embed!(destination::T, subject_pair::Pair{String, S}, elementor::Elementor = getElementor(); overwrite = false) where {T <: GObject, S <: GObject}
	(subject_name, subject) = subject_pair

	push!(destination, subject)
	push!(elementor, subject_pair; overwrite = overwrite)
end

function embed!(destination::T, subject::S, elementor::Elementor = getElementor()) where {T <: GObject, S <: GObject}
	push!(destination, subject)
	push!(elementor, subject)
end

embed(object::T, elementor::Elementor = getElementor()) where T <: GObject = push!(elementor, object)
embed(object_pair::Pair{String, T}, elementor::Elementor = getElementor(); overwrite = false, nameless = false) where T <: GObject = push!(elementor, object_pair; overwrite = overwrite, nameless = nameless)

# TODO: create wrapper functions for embedding pages to the GtkStack

function letgo(key::String, elementor::Elementor = getElementor(); remove = false)
	if !(key ∈ keys(elementor.objects))
		error("Cannot remove object with the name $key as it does NOT exist! Aborting...")
	end

	object = getElement(key)
	if remove
		destroy(object)
	end
	delete!(elementor.objects, key)
end

function letgo(object::T, elementor::Elementor = getElementor(); remove = false) where T <: GObject
	object_name = get_gtk_property(object, :name, String)

	if object_name === ""
		if remove
			destroy(object)
		else
			error("Cannot remove nameless object from elementor Elementor! Aborting...")
		end
	end

	letgo(object_name, elementor; remove = remove)
end