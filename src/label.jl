using Gtk

function changeLabel!(label::GObject, labelText::AbstractString)
	set_gtk_property!(label, :label, labelText)
end

changeLabel!(label::AbstractString, labelText::AbstractString) = changeLabel!(getElement(label), labelText)

function changeSubtitle!(header::GObject, subtitleText::AbstractString)
	set_gtk_property!(header, :subtitle, subtitleText)
end

changeSubtitle!(header::AbstractString, subtitleText::AbstractString) = changeSubtitle!(getElement(header), subtitleText)
