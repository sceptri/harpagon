import JSON

const preferences_filename = "preferences.json"
const default_preferences = let NS = Namespace
	Dict(
		NS.settings[NS.Page.key] 		=> NS.settings[NS.Page.properties],
		NS.overview[NS.Page.key] 		=> NS.overview[NS.Page.properties],
		NS.terms[NS.Page.key] 			=> NS.terms[NS.Page.properties],
		NS.transactions[NS.Page.key] 	=> NS.transactions[NS.Page.properties],
		NS.subjects[NS.Page.key] 		=> NS.subjects[NS.Page.properties],
		NS.accounts[NS.Page.key] 		=> NS.accounts[NS.Page.properties],
		NS.categories[NS.Page.key] 		=> NS.categories[NS.Page.properties],
		NS.tags[NS.Page.key] 			=> NS.tags[NS.Page.properties],
		NS.categorization[NS.Page.key]	=> NS.categorization[NS.Page.properties],
		NS.budgets[NS.Page.key]			=> NS.budgets[NS.Page.properties],
		NS.budgetting[NS.Page.key]		=> NS.budgetting[NS.Page.properties]
	)
end

NothingOrVector = Union{Nothing, Vector}

mutable struct Preferences
	save::SaveSystem
	pages::NothingOrVector

	# Make preferences a sort of singleton
	function Preferences(create_new::Bool = false)
		# Returns global preferences object if it is defined
		if (@isdefined preferences) && !create_new
			return getGlobalPreferences()
		end

		# Otherwise it crates a new one
		global home_location
		global preferences_filename

		if !isdir(home_location)
			mkdir(home_location)
		end

		global default_JSON_file
		path = home_location * '/' * preferences_filename
		if !isfile(path)
			io = open(path, "w")
			write(io, JSON.json(default_preferences)) # default JSON file
			close(io)
		end

		preferences = new(SaveSystem("json", "r+"), nothing)
		openSave!(preferences.save, path)
		updatePages!(preferences)
		
		return preferences
	end
end

# TODO: update preferences function
function getGlobalPreferences()
	global preferences

	if !@isdefined preferences
		error("Global preferences object is not defined")
	end

	return preferences
end

isopen(preferences::Preferences) = isopen(preferences.save) && preferences.categories !== nothing

function updatePages!(preferences::Preferences)
	let stack_page = Namespace.Page.stack_page
		filtered = preferences.save.contents |> content -> filter(page -> haskey(last(page), stack_page) ? last(page)[stack_page] : false, content) 
		preferences.pages = filtered |> keys |> collect
	end
end
# TODO: add function to applied overrides from save

function getPagesIn(preferences::Preferences, page_name::String)
	let in_page = Namespace.Page.in_page
		filtered = preferences.save.contents |> content -> filter(page -> haskey(last(page), in_page) ? (last(page)[in_page] == page_name) : false, content) 
		filtered |> keys |> collect
	end
end