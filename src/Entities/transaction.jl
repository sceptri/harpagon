using Dates

Base.@kwdef mutable struct Transaction <: AbstractEntity
	core::Entity = Entity()
	date::DateTime = now()
	amount::Float64 = 0.0 # all saved amounts must be positive - if amount < 0, then it means that payee and payer must be switched
	payer::String = "" # payer subject id - the FROM
	payee::String = "" # payee subject id - the TO
	category::String = "" # category id
	tags::Vector{String} = [] # tag ids
	type::String = "" # id of payment type 	| from PREFERENCES
	status::String = "" # id of status | from PREFERENCES
	note::String = ""
end

Transaction(core::Entity; date, amount, payer, payee, category, tags, type, status, note) = 
	Transaction(;core = core, date = date, amount = amount, payer = payer, payee = payee, 
		category = category, tags = tags, type = type, status = status, note = note)

function getkey(::Type{Transaction})
	return :transactions
end