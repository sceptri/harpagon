Base.@kwdef mutable struct Subject <: AbstractEntity
	core::Entity = Entity()
	name::String = ""
	accounts::Vector{String} = [] # account ids (only toplevel accounts)
	note::String = ""
	tags::Vector{String} = [] # tag ids
	category::String = "" # category id
	owned::Bool = false # owned subjects are shown in overview etc. - belong to the user
end

Subject(core::Entity; name, accounts, note, tags, category, owned) = 
	Subject(;core = core, name = name, accounts = accounts, note = note, tags = tags, category = category, owned = owned)

function getkey(::Type{Subject})
	return :subjects
end