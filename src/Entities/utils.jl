using Dates

import Base: convert

convert(::Type{DateTime}, value::String) = DateTime(value)
convert(::Type{NumberType}, value::String) where NumberType <: Number = parse(NumberType, value)