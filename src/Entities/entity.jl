# Supertype for all other entities
abstract type AbstractEntity end

import ..SaveSystem, ..Namespace, ..getGlobalSaveSystem, ..DEBUG
import Base: convert

const TEMPORARY_ID = "temporary_id"

Base.@kwdef mutable struct Entity <: AbstractEntity
	id::String = TEMPORARY_ID
	in_save::SaveSystem = SaveSystem()
end

# Entity type to indicate that no type should be used
Base.@kwdef mutable struct NullEntity <: AbstractEntity end

# Importing function definitions, so we add ONLY the methods
import Base: isempty, getproperty, getkey, propertynames

# TODO: add isvalid and getcore function

# overload getkey for use with specific/entity
function getkey(::Type{T}) where T <: AbstractEntity
	return nothing
end

function keywordargs(::Type{EntityType}, payload::Dict{String, Any}) where EntityType <: AbstractEntity
	symbolic_payload = Dict{Symbol, Any}()
	for (key, value) in payload
		symbolic_key = Symbol(key)

		if hasfield(EntityType, symbolic_key)
			symbolic_payload[symbolic_key] = value
		end
	end

	return symbolic_payload
end

function query(::Type{EntityType}, save_system::SaveSystem) where EntityType <: AbstractEntity
	by = getkey(EntityType)
	if by === nothing || !isdefined(Namespace, by)
		error("Cannot query by nonexistent key!")
	end

	all_entities = let entity_key = getproperty(Namespace, by)[Namespace.Page.key]
		save_system.contents[entity_key]
	end

	if isempty(all_entities)
		return Vector{EntityType}()
	end

	entities = Vector{EntityType}()
	for (key, object) in all_entities
		object = sanitizeType(object)

		entity = EntityType(Entity(key, save_system); keywordargs(EntityType, object)...)
		push!(entities, entity)
	end

	return entities
end

query(::Type{EntityType}) where EntityType <: AbstractEntity = query(EntityType, getGlobalSaveSystem())

function sanitizeType(object::Dict{String, Any})
	object = copy(object)
	if haskey(object, "type")
		try
			object["type"] = convert(DataType, object["type"])
		catch end
	end

	return object
end

convert(::Type{DataType}, x::StringType) where StringType <: AbstractString = Symbol(x) |> eval

function findby(::Type{EntityType}, id::String, save_system::SaveSystem) where EntityType <: AbstractEntity
	by = getkey(EntityType)
	if by === nothing || !isdefined(Namespace, by)
		error("Cannot query by nonexistent key!")
	end

	all_entities = let entity_key = getproperty(Namespace, by)[Namespace.Page.key]
		save_system.contents[entity_key]
	end

	if isempty(all_entities)
		return EntityType(; core = Entity(id, save_system))
	end

	if haskey(all_entities, id)
		entity = sanitizeType(all_entities[id])

		return EntityType(Entity(id, save_system); keywordargs(EntityType, entity)...)
	else
		return EntityType(; core = Entity(id, save_system))
	end
end

findby(type::Type{EntityType}, id::String) where EntityType <: AbstractEntity = findby(type, id, getGlobalSaveSystem())


function isempty(entity::T) where T <: AbstractEntity
	# special case for generic entity
	if T == Entity
		return entity.id === "" || !isopen(entity.in_save)
	end

	if !hasfield(T, core)
		error("Type $T MUST implement Entity as `core` field!")
	end

	return entity.core.id === "" || !isopen(entity.core.in_save)
end

isempty(entity::NullEntity) = false # NullEntity is never empty

function setproperty(entity::EntityType, field_name::Symbol, value::Any; try_convert = true) where EntityType <: AbstractEntity
	if !hasfield(EntityType, field_name)
		error("Entity $EntityType has no field named $field_name")
	end

	field_value = getfield(entity, field_name)

	if typeof(field_value) != typeof(value)
		now_ok = false
		if try_convert
			(now_ok, value) = attempt_convert(field_value, value)
		end

		if !now_ok
			error("Type of $field_name - $(typeof(field_value)) and value - $(typeof(value)) ($value) are NOT the same")
		end
	end

	try
		setfield!(entity, field_name, value)
	catch
		keywords = getproperties(entity)
		keywords[field_name] = value

		generic_entity = Base.typename(EntityType).wrapper

		entity = generic_entity(; core = entity.core, keywords...)
	end
	
	if !hasfield(EntityType, :core)
		error("Type $EntityType MUST implement Entity as `core` field!")
	end

	save_system = entity.core.in_save

	by = getkey(EntityType)
	if by === nothing || !isdefined(Namespace, by)
		error("Cannot query by nonexistent key!")
	end

	if typeof(value) <: Type
		value =  split(string(value), '.') |> last
	end

	save_system.contents[getproperty(Namespace, by)[Namespace.Page.key]][entity.core.id][String(field_name)] = value

	return entity
end

function attempt_convert(field_value, value)
	now_ok = false

	try
		if typeof(field_value) <: Array
			value = Meta.parse(value) |> eval
		elseif typeof(field_value) <: Type
			value = convert(DataType, value)
		else
			value = convert(typeof(field_value), value)
		end
		
		now_ok = true
	catch
		DEBUG && println("Warning! Could NOT convert $value of type $(typeof(value)) to type $(typeof(field_value))")
		now_ok = false
	end

	return (now_ok, value)
end

function getproperty(entity::EntityType, property_name::Symbol; simplify = false, simplify_only_type = true) where EntityType <: AbstractEntity
	if !hasfield(EntityType, property_name)
		error("Cannot get nonexistent property $(String(property_name)) from $EntityType")
	end

	value = getfield(entity, property_name)

	original_type = typeof(value)
	if simplify_only_type && simplifytype(original_type) && original_type <: Type
		value = split(string(value), '.') |> last
	elseif simplify && simplifytype(original_type)
		value = string(value)
		if original_type <: Type
			value = split(value, '.') |> last
		end
	end

	return value
end

function getproperties(entity::EntityType, ignore_core = true; only_values = false, simplify = false, simplify_only_type = true, string_keys = false) where EntityType <: AbstractEntity
	key_type = string_keys ? String : Symbol

	properties = only_values ? Vector{Any}() : Dict{key_type, Any}()
	for property_name in fieldnames(EntityType)
		if property_name == :core && ignore_core
			continue
		end

		value = getproperty(entity, property_name; simplify, simplify_only_type)

		if only_values
			push!(properties, value)
		else
			properties[string_keys ? string(property_name) : property_name] = value	
		end	
	end

	return properties
end

function getid(entity::EntityType) where EntityType <: AbstractEntity
	if EntityType == Entity
		return entity.id
	end

	if !hasfield(EntityType, :core)
		error("Type $EntityType MUST implement Entity as `core` field!")
	end

	return entity.core.id
end

getid(entity::NullEntity) = nothing # NullEntity does NOT hold any ID

function simplifytype(type::Type)
	return 	type <: AbstractArray 	||
			type <: DateTime 		|| 
			type <: Type
end

function uneditabletype(type::Type)
	return false
end

function propertytypes(::Type{EntityType}, ignore_core::Bool = true; simplify = false) where EntityType <: AbstractEntity
	if EntityType == Entity
		return [fieldtypes(EntityType)...]
	end

	types = [fieldtypes(EntityType)...]

	if ignore_core
		popfirst!(types) # remove first type - which SHOULD be entity
	end

	if simplify
		types = map(type -> (simplifytype(type) ? String : type), types)
	end

	return types
end

propertytypes(::Type{NullEntity}, ignore_core::Bool = true; simplify = false) = nothing # NullEntity does NOT contain any fields

function propertytype(name::Symbol, type::Type{EntityType}, ignore_core::Bool = true; simplify = false) where EntityType <: AbstractEntity
	names = propertynames(type, ignore_core)
	types = propertytypes(type, ignore_core; simplify = simplify)

	return types[name .== names] |> first
end

function propertynames(::Type{EntityType}, ignore_core::Bool = true) where EntityType <: AbstractEntity
	if EntityType == Entity
		return [fieldnames(EntityType)...]
	end

	types = [fieldnames(EntityType)...]

	if ignore_core
		popfirst!(types) # remove first name - which SHOULD be entity
	end


	return types
end

propertynames(::Type{NullEntity}, ignore_core::Bool = true) = nothing # NullEntity does NOT contain any fields

# TODO: write function to correctly fix temporary ids to new entities
function processTemporaryEntity!(entity::EntityType) where EntityType <: AbstractEntity
	if !occursin(TEMPORARY_ID, entity.core.id)
		return
	end

	index = 0
	entity.core.id = string(TEMPORARY_ID, '_', index) 

	base_type = Base.typename(EntityType).wrapper
	all_entities = query(base_type, entity.core.in_save)

	while !unique_temporary_id(entity, all_entities)
		index += 1
		entity.core.id = string(TEMPORARY_ID, '_', index) 
	end

	by = getkey(base_type)
	if by === nothing || !isdefined(Namespace, by)
		error("Cannot query by nonexistent key!")
	end

	entity.core.in_save.contents[getproperty(Namespace, by)[Namespace.Page.key]][entity.core.id] = getproperties(entity; string_keys = true)
end

function unique_temporary_id(entity::EntityType, all_entities) where EntityType <: AbstractEntity
	for iteration in all_entities
		if iteration.core.id == entity.core.id
			return false
		end
	end

	return true
end