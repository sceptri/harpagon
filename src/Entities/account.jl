Base.@kwdef mutable struct Account <: AbstractEntity
	core::Entity = Entity()
	parent::String = "" # id of the ACCOUNT it belongs to
	name::String = ""
	note::String = ""
	category::String = "" # category id - Must be an instance of one of the defined categories for an account
	tags::Vector{String} = [] # tag ids
	balance::Float64 = 0.0
	start_balance::Float64 = 0.0
	type::String = "" # id of account type | from PREFERENCES
	closed::Bool = false
end

Account(core::Entity; parent, name, note, category, tags, balance, start_balance, type, closed) = 
	Account(;core = core, parent = parent, name = name, note = note, category = category, 
		tags = tags, balance = balance, start_balance = start_balance, type = type, closed = closed)

function getkey(::Type{Account})
	return :accounts
end