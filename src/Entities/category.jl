# Entity can have ONLY ONE category
Base.@kwdef mutable struct Category{EntityType <: AbstractEntity} <: AbstractEntity
	core::Entity = Entity()
	name::String = ""
	type::Type{EntityType} = Entity
	parent::String = "" # id - parent category must be of the same entity
	note::String = ""
end

Category(core::Entity; name, type, parent, note) = 
	Category(;core = core, name = name, type = type, parent = parent, note = note)

function getkey(::Type{CategoryType}) where CategoryType <: Category
	return :categories
end