using Dates

Base.@kwdef mutable struct Term <: AbstractEntity
	core::Entity = Entity()
	name::String = ""
	budgets::Vector{String} = [] # budget ids applied to the term
	date_from::DateTime = now()
	date_to::DateTime = now()
	category::String = "" # category id
	tags::Vector{String} = [] # tag ids
	note::String = ""
end

Term(core::Entity; name, budgets, date_from, date_to, category, tags, note) = 
	Term(;core = core, name = name, budgets = budgets, date_from = date_from, 
		date_to = date_to, category = category, tags = tags, note = note)

function getkey(::Type{Term})
	return :terms
end