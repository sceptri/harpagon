Base.@kwdef mutable struct Budget <: AbstractEntity
	core::Entity = Entity()
	name::String = ""
	identifier::String = "" # either Subject or Account or Tag{Transaction} or Category{Transaction}
	limit::Float64 = 0.0
	tolerance::Float64 = 0.0 # percentage tolerance of limit
	note::String = ""
end

# TODO: add check if identifier is of the correct entity
Budget(core::Entity; name, identifier, limit, tolerance, note) = 
	Budget(;core = core, name = name, identifier = identifier, limit = limit, tolerance = tolerance, note = note)

function getkey(::Type{Budget})
	return :budgets
end