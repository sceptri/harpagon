# Entity can have multiple tags
Base.@kwdef mutable struct Tag{EntityType <: AbstractEntity} <: AbstractEntity
	core::Entity = Entity()
	name::String = ""
	type::Type{EntityType} = Entity
	parent::String = "" # id - parent tag must be of the same entity
	note::String = ""
end

Tag(core::Entity; name, type, parent, note) = 
	Tag(;core = core, name = name, type = type, parent = parent, note = note)

function getkey(::Type{TagType}) where TagType <: Tag
	return :tags
end