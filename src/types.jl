import Gtk: suffix, @gtktype_custom_symname,
	@gtktype_custom_symname_and_lib, @Gtype, _gtksubtype_constructors, GtkWidget, AbstractStringLike,
	push!, insert!, GObject, Gtk


const libgtk = Gtk.libgtk

# TODO: fix GtkStack type definition on reload of module
try
	Gtk.@gtktype GtkStack
catch exception
	println("GtKStack already defined! Skiping...")
end

GtkStackLeaf() = GtkStackLeaf(ccall((:gtk_stack_new, libgtk), Ptr{GObject}, ()))

# TODO: More Julian function signature for push/insert
# Add named page
function insert!(stack::GtkStack, page::Union{GtkWidget, AbstractStringLike}, name::AbstractStringLike)
	ccall((:gtk_stack_add_named, libgtk), Cvoid,
		(Ptr{GObject}, Ptr{GObject}, Ptr{Cchar}),
		stack, page, name)
	stack
end

push!(stack::GtkStack, page::Union{GtkWidget, AbstractStringLike}, name::AbstractStringLike) = insert!(stack, page, name)

# Add titled page
function insert!(stack::GtkStack, page::Union{GtkWidget, AbstractStringLike}, name::AbstractStringLike, title::AbstractStringLike)
	ccall((:gtk_stack_add_titled, libgtk), Cvoid,
		(Ptr{GObject}, Ptr{GObject}, Ptr{Cchar}, Ptr{Cchar}),
		stack, page, name, title)
	stack
end

push!(stack::GtkStack, page::Union{GtkWidget, AbstractStringLike}, name::AbstractStringLike, title::AbstractStringLike) = insert!(stack, page, name, title)

# TODO: add function gtk_stack_get_child_by_name

# TODO: fix GtkStackSwitcher type definition on reload of module
try
	Gtk.@gtktype GtkStackSwitcher
catch exception
	println("GtkStackSwitcher already defined! Skiping...")
end

GtkStackSwitcherLeaf() = GtkStackSwitcherLeaf(ccall((:gtk_stack_switcher_new, libgtk), Ptr{GObject}, ()))

function set_stack!(stack_switcher::GtkStackSwitcher, stack::GtkStack)
	ccall((:gtk_stack_switcher_set_stack, libgtk), Cvoid,
		(Ptr{GObject}, Ptr{GObject}),
		stack_switcher, stack)
end

function get_stack(stack_switcher::GtkStackSwitcher)
	GtkStackLeaf(ccall((:gtk_stack_switcher_get_stack, libgtk), Ptr{GObject},
		(Ptr{GObject},), stack_switcher))
end

# TODO: add function to change stack page gtk_stack_set_visible_child