import JSON
using Dates

NothingOrDict = Union{Nothing, Dict{String, Any}}

const persisted_save_location = homedir() * "/.harpagon"
const persisted_save_filename = "opened_saves.json"
const default_JSON_file = "{}"
const default_file_suffix = "hpg" # Custom suffix even though it is saved as JSON

const default_save = let NS = Namespace
	Dict(
		NS.overview[NS.Page.key] 		=> Dict{String, Any}(),
		NS.terms[NS.Page.key] 			=> Dict{String, Any}(),
		NS.transactions[NS.Page.key] 	=> Dict{String, Any}(),
		NS.subjects[NS.Page.key] 		=> Dict{String, Any}(),
		NS.accounts[NS.Page.key]		=> Dict{String, Any}(),
		NS.categories[NS.Page.key] 		=> Dict{String, Any}(),
		NS.tags[NS.Page.key] 			=> Dict{String, Any}(),
		NS.override[NS.Page.key]		=> Dict{String, Any}(),
		NS.budgets[NS.Page.key]			=> Dict{String, Any}()
	)
end
mutable struct SaveSystem
	contents::NothingOrDict
	suffix::String
	mode::String
	with_suffix::Bool
	filename::Union{Nothing, String}
	created::Bool

	SaveSystem(suffix = default_file_suffix, mode = "r+"; with_suffix = true, created = false) = begin
		contents = created ? Dict{String, Any}() : nothing
		return new(contents, suffix, mode, with_suffix, nothing, created)
	end
end

isopen(save_system::SaveSystem) = (save_system.contents !== nothing && save_system.filename !== nothing)

function getGlobalSaveSystem()::SaveSystem
	global save_system

	if !@isdefined save_system
		error("Global save_system object is not defined")
	end

	return save_system
end

function writeSave(save_system::SaveSystem; save_as = false, replace_temporary = false)
	if replace_temporary
		replaceTemporaryIds(save_system)
	end

	iostream = open(save_system.filename, "w")
	write(iostream, JSON.json(save_system.contents))
	close(iostream)

	if !save_as
		save_system.created = false
	end

	reloadSave!(save_system)
end

function openSave!(save_system::SaveSystem, filename::AbstractString)::Bool
	filename = save_system.with_suffix ? filename : string(filename, '.' * save_system.suffix)
	if !isfile(filename)
		return false
	end

	file_contents = open(filename, save_system.mode) |> s -> read(s, String)

	save_system.filename = filename
	save_system.contents = JSON.parse(file_contents)

	return true
end

openSave(filename::AbstractString) = openSave!(getGlobalSaveSystem(), filename)
reloadSave!(save_system::SaveSystem) = openSave!(save_system, save_system.filename)

function persistSave!(persisted_save_system::SaveSystem, save_system::SaveSystem)
	# Throw error is either of save systems is closed (so we can't write into it or read from it)
	if !isopen(save_system) || !isopen(persisted_save_system)
		error("Both save systems MUST be opened!")
	end

	opened_saves = persisted_save_system.contents
	opened_saves[save_system.filename] = now()

	writeSave(persisted_save_system)
end

persistSave(save_system::SaveSystem = getGlobalSaveSystem()) = persistSave!(makeOrLoadPersistedSave(), save_system)


function makeOrLoadPersistedSave()::SaveSystem
	global home_location
	global persisted_save_filename

	if !isdir(home_location)
		mkdir(home_location)
	end

	global default_JSON_file
	path = home_location * '/' * persisted_save_filename
	if !isfile(path)
		io = open(path, "w")
		write(io, default_JSON_file) # default JSON file
		close(io)
	end

	persisted_save_system = SaveSystem("json", "r+")
	openSave!(persisted_save_system, path)
	return persisted_save_system
end

function openLastSave!(save_system::SaveSystem)
	persisted_save = makeOrLoadPersistedSave()
	last_date = nothing
	last_save_name = nothing

	if isempty(persisted_save.contents)
		return false
	end

	for (save, date) in persisted_save.contents
		if last_date === nothing
			last_date = date
			last_save_name = save
			continue
		end

		if DateTime(last_date) < DateTime(date)
			last_date = date
			last_save_name = save
		end
	end

	response = openSave!(save_system, last_save_name)
	# If the file could not be opened, retry again
	if !response
		DEBUG && println("Saved file $last_save_name not found! Removing...")

		delete!(persisted_save.contents, last_save_name)
		writeSave(persisted_save)
		openLastSave!(save_system)
	end

	return true
end

openLastSave() = openLastSave!(getGlobalSaveSystem())

function clearSave!(save_system::SaveSystem)
	save_system.contents = nothing
	save_system.filename = nothing
end

clearSave() = clearSave!(getGlobalSaveSystem())

function newSave!(save_system::SaveSystem, write_defaults = true; close_save = false)
	if isopen(save_system) && !close_save
		error("Cannot create new save when another one is opened!")
	end

	clearSave!(save_system)
	save_system.contents = write_defaults ? default_save : Dict{String, Any}()
	save_system.created = true
end

newSave(write_defaults = true; close_save = false) = newSave!(getGlobalSaveSystem(), write_defaults; close_save = close_save)

function replaceTemporaryIds(save_system::SaveSystem)
	for (type_key, type) in save_system.contents
		if !(typeof(type) <: Dict) || isempty(type)
			continue
		end

		for (key, entity) in type
			if occursin(TEMPORARY_ID, string(key))
				#IDs look really pretty this way, plus it is indicated, that they are written in hexadecimal
				new_key = "0x" * string(id_hash(string(key, values(entity)...)); base = 16) 

				save_system.contents[type_key][new_key] = save_system.contents[type_key][key]
				delete!(save_system.contents[type_key], key)
			end
		end
	end
end