using Gtk

const default_stylesheet = @path joinpath(@__DIR__, "../styles.css")	

NothingOrCssProvider = Union{Nothing, GtkCssProviderLeaf}
mutable struct Styler
	css_contents::String
	mode::String
	provider::NothingOrCssProvider

	Styler(css_contents = "", mode = "r", provider = nothing) = new(css_contents, mode, provider)
end

function getGlobalStyler()
	global styler

	if !@isdefined styler
		error("Global styler object is not defined")
	end

	return styler
end

isopen(styler::Styler) = styler.provider !== nothing

function loadProvider!(styler::Styler)
	if !isopen(styler)
		loadCssFile!(styler)
	end
end

function loadCssFile!(styler::Styler, style_file_name::AbstractString = default_stylesheet)
	styler.css_contents = open(style_file_name, styler.mode) |> s -> read(s, String)
	styler.provider = GtkCssProviderLeaf(data = styler.css_contents)
end

function applyStyle!(object::GObject; priority::Integer = 1000, styler::Styler = getGlobalStyler())
	loadProvider!(styler)

	styleContext = GAccessor.style_context(object)
	push!(styleContext, GtkStyleProvider(styler.provider), priority)
end