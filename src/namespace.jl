module Namespace
	# Page properties
	module Page
		const title = "title"
		const name = "name"
		const key = "key"
		const properties = "properties"
		const stack_page = "stack_page"
		const hamburger_menu = "hamburger_menu"
		const in_page = "in_page"
	end

	# Pages
	const settings = Dict(
		Page.key 		=> "Settings",
		Page.properties => Dict(
			Page.title 			=> "Settings",
			Page.name 			=> "SettingsBox",
			Page.hamburger_menu	=> true,
		)
	)
	const overview = Dict(
		Page.key 		=> "Overview",
		Page.properties => Dict(
			Page.title 		=> "Overview",
			Page.name 		=> "OverviewBox",
			Page.stack_page => true
		)
	)

	const budgetting = Dict(
		Page.key 		=> "Budgetting",
		Page.properties	=> Dict(
			Page.title		=> "Budgetting",
			Page.name		=> "BudgettingBox",
			Page.stack_page	=> true
		)
	)

	const terms = Dict(
		Page.key 		=> "Terms",
		Page.properties => Dict(
			Page.title 		=> "Terms",
			Page.name 		=> "TermsBox",
			Page.in_page 	=> budgetting[Page.key]
		)
	)
	const transactions = Dict(
		Page.key 		=> "Transactions",
		Page.properties => Dict(
			Page.title 		=> "Transactions",
			Page.name 		=> "TransactionsBox",
			Page.stack_page => true
		)
	)
	const subjects = Dict(
		Page.key 		=> "Subjects",
		Page.properties => Dict(
			Page.title 		=> "Subjects",
			Page.name 		=> "SubjectsBox",
			Page.stack_page => true
		)
	)

	const categorization = Dict(
		Page.key 		=> "Categorization",
		Page.properties => Dict(
			Page.title 		=> "Categorization",
			Page.name 		=> "CategorizationBox",
			Page.stack_page => true
		)
	)

	const categories = Dict(
		Page.key 		=> "Categories",
		Page.properties => Dict(
			Page.title 		=> "Categories",
			Page.name 		=> "CategoriesBox",
			Page.in_page 	=> categorization[Page.key]
		)
	)
	const tags = Dict(
		Page.key 		=> "Tags",
		Page.properties => Dict(
			Page.title 		=> "Tags",
			Page.name 		=> "TagsBox",
			Page.in_page 	=> categorization[Page.key]
		)
	)

	const override = Dict(
		Page.key		=> "Override",
		Page.properties	=> Dict()
	)

	const accounts = Dict(
		Page.key 		=> "Accounts",
		Page.properties	=> Dict(
			Page.title 		=> "Accounts",
			Page.name 		=> "AccountsBox",
			Page.in_page	=> subjects[Page.key]
		)
	)

	const budgets = Dict(
		Page.key		=> "Budgets",
		Page.properties	=> Dict(
			Page.title 		=> "Budgets",
			Page.name 		=> "BudgetsBox",
			Page.in_page 	=> budgetting[Page.key]
		)
	)

	

	# TODO: add account types, payment types and payment statuses
end