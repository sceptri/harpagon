module Harpagon
	using Gtk
	using RelocatableFolders

	# TODO: script for copying glade file contents to a source file before compilation
	const gladeFile = @path joinpath(@__DIR__, "../harpagon.glade")
	const home_location = homedir() * "/.harpagon"
	# Debug flag
	const DEBUG = true
	const APP_TITLE = "Harpagon"
	const APP_ICON = @path joinpath(@__DIR__, "../temporary-icon.png")

	# Define hash function used throught Harpagon
	# It's purpose is to generate same-looking ids, not to enhance security
	const id_hash = Base.hash
	
	# Order matters!
	include("utils.jl")
	include("namespace.jl")
	include("types.jl")
	include("builder.jl")
	include("elementor.jl")
	include("saveSystem.jl")
	include("preferences.jl")
	include("styler.jl")
	include("label.jl")

	# Export all the symbols for easier operations with them in submodules
	include("exports.jl")

	include("Entities/Entities.jl")
	using .Entities

	include("Pages/Pages.jl")
	using .Pages

	const builder = Builder()
	const elementor = Elementor()
	const save_system = SaveSystem()
	const styler = Styler()
	const preferences = Preferences()

	const windowPage = Window("MainStack", "HeaderSwitcher")
	
	const subjectsPage = Subjects("subjects", Subject)
	const transactionsPage = Transactions("transactions", Transaction)

	const categorizationPage = Categorization("CategorizationBox")
	const budgettingPage = Budgetting("BudgettingBox")
	
	const termsPage = Terms("terms", Term)
	const budgetsPage = Budgets("budgets", Budget)

	const categoriesPage = Categories("categories", Category)
	const tagsPage = Tags("tags", Tag)

	function julia_main()::Cint
		onLoad()
		openLastSaveIfPossible()

		# TODO: remove subjectsPage & transactionsPage as globals and just pass them to the closeSave
		signal_connect(WindowUtils.openFile, getElement("HeaderFileChooser"), :clicked, Nothing, (), false, (subjectsPage, transactionsPage, categoriesPage, tagsPage, termsPage, budgetsPage))
		signal_connect(WindowUtils.closeSave, getElement("HeaderCloseButton"), :clicked, Nothing, (), false, (subjectsPage, transactionsPage, categoriesPage, tagsPage, termsPage, budgetsPage))
		signal_connect(WindowUtils.doSave, getElement("HeaderSaveButton"), :clicked, Nothing, (), false, (subjectsPage, transactionsPage, categoriesPage, tagsPage, termsPage, budgetsPage))
		signal_connect(WindowUtils.createSave, getElement("HeaderNewButton"), :clicked, Nothing, (), false, (subjectsPage, transactionsPage, categoriesPage, tagsPage, termsPage, budgetsPage))
		signal_connect(do_debug_procedure, getElement("HeaderBurgerButton"), :clicked, Nothing, (), false, nothing)

		# If it's not ran in REPL, this should take care of the closing etc.
		# TODO: polish non-iteractive mode
		if !isinteractive()
			condition = Condition()

			@async Gtk.gtk_main()

			destroyCallback(widget, user_data)::Nothing = begin	
				notify(condition)
			end

			signal_connect(destroyCallback, getElement("MainWindow"), :destroy, Nothing, (), false, nothing)
			
			wait(condition)
			Gtk.gtk_quit();
		end

		return 0
	end

	function onLoad()
		builder.instance = GtkBuilder(filename = gladeFile)
		importFromBuilder!(elementor, builder)

		global APP_TITLE
		set_gtk_property!(getElement("MainWindow"), :title, APP_TITLE)

		preferences = getGlobalPreferences()
		updatePages!(preferences)

		# TODO: Reorder pages corresponding to the correct permutation
		for key in preferences.pages		
			global windowPage

			title = preferences.save.contents[key][Namespace.Page.title]
			name = preferences.save.contents[key][Namespace.Page.name]

			current_subpage = TitledPage(name, title, :v)
			addSubpage!(windowPage, current_subpage)
		end

		global categorizationPage, budgettingPage
		createPage(categorizationPage)
		createPage(budgettingPage)

		getElement("MainWindow") |> showall
	end

	function openLastSaveIfPossible()
		save_system = getGlobalSaveSystem()
		openLastSave!(save_system)
		if isopen(save_system)
			global subjectsPage, transactionsPage, categoriesPage, tagsPage, termsPage, budgetsPage
			WindowUtils.showSave(save_system, (subjectsPage, transactionsPage, categoriesPage, tagsPage, termsPage, budgetsPage))
		end
	end

	function do_debug_procedure()
		pretty_print(getGlobalSaveSystem().contents)

		return nothing
	end

	do_debug_procedure(widget, parameters) = do_debug_procedure()
end

# Remove before compilation
Harpagon.julia_main()