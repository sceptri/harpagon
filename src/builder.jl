using Gtk

NothingOrBuilder = Union{Nothing, Gtk.GtkBuilder} 

mutable struct Builder
	instance::NothingOrBuilder

	function Builder(object::NothingOrBuilder = nothing)
		# Makes Builder(something) evaluate to error next time it is used
		eval(:(Builder(object) = error(
				"Cannot instantiate object twice")))

		return new(object)
	end
end

function getGlobalBuilder()::Builder
	global builder
	
	if !@isdefined builder
		error("Global builder object is not defined")
	end

	return builder
end

isopen(builder::Builder) = builder.instance !== nothing