Base.@kwdef mutable struct Budgets{T <: AbstractEntity} <: AbstractPageTree
	core::NamespacedCorePage{T}
	subpages::Dict{String, Any} = Dict{String, Any}()
end

Budgets(identifier::String, type::Type{T}) where T <: AbstractEntity = Budgets(; core = NamespacedCorePage(identifier, type))