import ..Harpagon: Namespace, getGlobalPreferences, pretty_print, getElement, getPagesIn

Base.@kwdef mutable struct Budgetting{CorePage <: ElementorCorePage} <: AbstractPageBox
	core::CorePage
	subpages::Dict{String,Any} = Dict{String,Any}()
end
suitableCore(pageType::Type{PageType}, element_name::String) where PageType <: Budgetting = ElementCorePage(element_name)
Budgetting(name::String) = Budgetting(; core = suitableCore(Budgetting, name)) 
Budgetting(name::String, orientation::Symbol) = Budgetting(; core = suitableCore(Budgetting, name, orientation)) 

function createPage(page::PageType) where PageType <: Budgetting
	preferences = getGlobalPreferences()

	name = Namespace.Page.name
	title = Namespace.Page.title
	key = Namespace.Page.key

	budgetting = Namespace.budgetting[key]

	for subpage_key in getPagesIn(preferences, budgetting)
		preferences_entry = preferences.save.contents[subpage_key]

		genericLabel = LabelPage("Label_" * preferences_entry[name], preferences_entry[title])
		addSubpage!(page, genericLabel)

		set_gtk_property!(getElement(genericLabel), :hexpand, true)

		genericBox = GenericPageBox(preferences_entry[name], :v)
		addSubpage!(page, genericBox)
	end
end