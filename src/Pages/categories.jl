Base.@kwdef mutable struct Categories{T <: AbstractEntity} <: AbstractPageTree
	core::NamespacedCorePage{T}
	subpages::Dict{String, Any} = Dict{String, Any}()
end

Categories(identifier::String, type::Type{T}) where T <: AbstractEntity = Categories(; core = NamespacedCorePage(identifier, type))