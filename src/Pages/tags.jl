Base.@kwdef mutable struct Tags{T <: AbstractEntity} <: AbstractPageTree
	core::NamespacedCorePage{T}
	subpages::Dict{String, Any} = Dict{String, Any}()
end

Tags(identifier::String, type::Type{T}) where T <: AbstractEntity = Tags(; core = NamespacedCorePage(identifier, type))