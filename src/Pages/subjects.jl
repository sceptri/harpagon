Base.@kwdef mutable struct Subjects{T <: AbstractEntity} <: AbstractPageTree
	core::NamespacedCorePage{T}
	subpages::Dict{String, Any} = Dict{String, Any}()
end

Subjects(identifier::String, type::Type{T}) where T <: AbstractEntity = Subjects(; core = NamespacedCorePage(identifier, type))