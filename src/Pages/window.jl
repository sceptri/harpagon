import ..Entities: Entity
Base.@kwdef mutable struct Window{T <: CorePage, S <: AbstractPage, P <: AbstractPage} <: AbstractPageStack
	core::T = BlankCorePage()
	subpages::Dict{String,Any} = Dict{String,Any}()
	stack_page::S = ElementPage("")
	switcher_page::P = ElementPage("")
end

Window(stack_name::String, switcher_name::String, subpages::Dict{String,Any} = Dict{String,Any}()) where T <: AbstractEntity = Window(; stack_page = ElementPage(stack_name), switcher_page = ElementPage(switcher_name), subpages = subpages)

module WindowUtils

using Gtk

import ...Harpagon: SaveSystem, getGlobalSaveSystem, persistSave, writeSave, newSave, getElement, applyStyle!, clearSave, openSave
import ..Pages: createPage, destroyPage, AbstractPage, addControls, refreshData!

function openFile(widget, parameters)::Nothing
	file = open_dialog("Select saved file", GtkNullContainer(), action = GtkFileChooserAction.OPEN)

	if file == ""
		return nothing
	end

	openSave(file)
	persistSave()

	showSave(parameters)

	return nothing
end

function showSave(save_system::SaveSystem, parameters::Any)
	subjectsPage = transactionsPage = categoriesPage = tagsPage = termsPage = budgetsPage = missing
	try
		subjectsPage, transactionsPage, categoriesPage, tagsPage, termsPage, budgetsPage = parameters
		@assert typeof(subjectsPage) <: AbstractPage && typeof(transactionsPage) <: AbstractPage &&
		typeof(categoriesPage) <: AbstractPage && typeof(tagsPage) <: AbstractPage && typeof(termsPage) <: AbstractPage
	catch
		error("Parameters supplied to showSave may (for now) only be pages")
	end

	createPage(subjectsPage)
	createPage(transactionsPage)
	createPage(categoriesPage)
	createPage(tagsPage)
	createPage(termsPage)
	createPage(budgetsPage)

	addControls(subjectsPage)
	addControls(transactionsPage)
	addControls(categoriesPage)
	addControls(tagsPage)
	addControls(termsPage)
	addControls(budgetsPage)

	show(getElement("HeaderSaveButton"))
	show(getElement("HeaderCloseButton"))

	show(getElement("HeaderSwitcher"))
	hide(getElement("HeaderTitle"))

	hide(getElement("HeaderFileChooser"))
	hide(getElement("HeaderNewButton"))

	applyStyle!(getElement("HeaderSaveButton"))
	applyStyle!(getElement("HeaderCloseButton"))
end

showSave(parameters::Any) = showSave(getGlobalSaveSystem(), parameters)

function doSave(parameters::Any)
	subjectsPage = transactionsPage = categoriesPage = tagsPage = termsPage = budgetsPage = missing
	try
		subjectsPage, transactionsPage, categoriesPage, tagsPage, termsPage, budgetsPage = parameters
		@assert typeof(subjectsPage) <: AbstractPage && typeof(transactionsPage) <: AbstractPage &&
		typeof(categoriesPage) <: AbstractPage && typeof(tagsPage) <: AbstractPage && typeof(termsPage) <: AbstractPage
	catch
		error("Parameters supplied to doSave may (for now) only be pages")
	end

	save_system = getGlobalSaveSystem()

	if save_system.created
		filename = save_dialog("Save as...", GtkNullContainer(), (GtkFileFilter("*.hpg, *.spendings", name="All supported formats"), "*.hpg", "*.spendings"))
		
		if !isa(filename, String)
			error("Filename must be a string!")
		end

		save_system.filename = filename
	end

	writeSave(save_system; replace_temporary = true)
	persistSave()

	refreshData!(subjectsPage)
	refreshData!(transactionsPage)
	refreshData!(categoriesPage)
	refreshData!(tagsPage)
	refreshData!(termsPage)
	refreshData!(budgetsPage)

	info_dialog("Your data have been saved!")

	return nothing
end

doSave(widget, parameters)::Nothing = doSave(parameters)

function closeSave(parameters::Any)
	subjectsPage = transactionsPage = categoriesPage = tagsPage = termsPage = budgetsPage = missing
	try
		subjectsPage, transactionsPage, categoriesPage, tagsPage, termsPage, budgetsPage = parameters
		@assert typeof(subjectsPage) <: AbstractPage && typeof(transactionsPage) <: AbstractPage &&
		typeof(categoriesPage) <: AbstractPage && typeof(tagsPage) <: AbstractPage && typeof(termsPage) <: AbstractPage
	catch
		error("Parameters supplied to closeSave may (for now) only be pages")
	end

	hide(getElement("HeaderSaveButton"))
	hide(getElement("HeaderCloseButton"))

	hide(getElement("HeaderSwitcher"))
	show(getElement("HeaderTitle"))

	show(getElement("HeaderFileChooser"))
	show(getElement("HeaderNewButton"))

	destroyPage(subjectsPage)
	destroyPage(transactionsPage)
	destroyPage(categoriesPage)
	destroyPage(tagsPage)
	destroyPage(termsPage)
	destroyPage(budgetsPage)

	clearSave()

	return nothing
end

closeSave(widget, parameters)::Nothing = closeSave(parameters)

createSave(widget, parameters)::Nothing = begin 
	newSave()
	showSave(getGlobalSaveSystem(), parameters)
	return nothing
end

end