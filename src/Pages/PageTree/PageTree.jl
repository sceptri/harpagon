module PageTree

using Gtk

import ..Pages: NamespacedCorePage, AbstractPageComposite, AbstractPage, createPage, destroyPage, isvalid, getcore, isNamespaced, ButtonPage, isCreateable, createPageFrom
import ...Harpagon: SaveSystem, Namespace, getElement, embed, embed!, letgo, getGlobalSaveSystem
import ...Entities: AbstractEntity, Entity, getproperties, setproperty, propertytypes, propertynames, getid, query, findby, propertytype, uneditabletype, processTemporaryEntity!

export AbstractPageTree, createPage, destroyPage, addControls, refreshData!

abstract type AbstractPageTree <: AbstractPageComposite end

function CellTextCallback(widget, row, value, page)
	column = convert(Int64, GAccessor.data(widget, "columnNumber"))
	row = parse(Int, row);

	store = getElement(page.core.identifier * "ListStore")
	id = store[row + 1, 1]

	subject = findby(page.core.type, id)
	field = propertynames(page.core.type)[column]
	subject = setproperty(subject, field, value)

	# the value could be converted to match the type, so use that
	store[row + 1, column + 1] = getproperty(subject, field; simplify = true)

	return nothing
end

function CellToggleCallback(widget, row, page)
	column = convert(Int64, GAccessor.data(widget, "columnNumber"))
	row = parse(Int, row);

	store = getElement(page.core.identifier * "ListStore")
	id = store[row + 1, 1]

	subject = findby(page.core.type, id)
	field = propertynames(page.core.type)[column]

	previous_value = getproperty(subject, field; simplify = true)
	subject = setproperty(subject, field, !previous_value)

	store[row + 1, column + 1] = getproperty(subject, field; simplify = true)

	return nothing
end

const TEXT_CELL = (() -> GtkCellRendererText(), :editable, "text", :edited, CellTextCallback, true)
const TOGGLE_CELL = (() -> GtkCellRendererToggle(), :activatable,"active", :toggled, CellToggleCallback, false)

function refreshData!(page::PageType) where PageType <: AbstractPageTree
	listStore = getElement(page.core.identifier * "ListStore")
	empty!(listStore)

	for entity in query(page.core.type)
		payload = (getid(entity), getproperties(entity; only_values = true, simplify = true)...)
		push!(listStore, payload)
	end
end

function createPage(page::PageType, save_system::SaveSystem = getGlobalSaveSystem()) where PageType <: AbstractPageTree
	isvalid(page) && typeof(page.core) <: NamespacedCorePage

	listStore = GtkListStore(String, propertytypes(page.core.type; simplify = true)...)
	embed(page.core.identifier * "ListStore" => listStore; overwrite = true, nameless = true)
	
	for entity in query(page.core.type)
		payload = (getid(entity), getproperties(entity; only_values = true, simplify = true)...)
		push!(listStore, payload)
	end

	treeView = GtkTreeView(GtkTreeModel(listStore))

	renderText = GtkCellRendererText()
	columnKey = GtkTreeViewColumn("Key", renderText, Dict("text" => 0))

	valueColumns = []
	editables = []
	columnIndex = 1
	for column_name in propertynames(page.core.type)
		if column_name == :core # TODO: add core as constant
			continue
		end

		is_editable = !(propertytype(column_name, page.core.type) |> uneditabletype)

		(cellTypeFunction, cellEditable, cellField, cellCallbackFlag, cellCallback, longCallback) = TEXT_CELL
		isBool = propertytype(column_name, page.core.type; simplify = true) == Bool
		if isBool
			(cellTypeFunction, cellEditable, cellField, cellCallbackFlag, cellCallback, longCallback) = TOGGLE_CELL
			cellTypeFunction = GtkCellRendererToggle
		end

		cellType = cellTypeFunction()

		if is_editable
			set_gtk_property!(cellType, cellEditable, true)
			GAccessor.data(cellType, "columnNumber", Ptr{Int64}(columnIndex))
		end

		columnValue = GtkTreeViewColumn(String(column_name), cellType, Dict(cellField => columnIndex))
		push!(valueColumns, columnValue)

		if is_editable
			push!(editables, (cellType, cellCallbackFlag, cellCallback, longCallback))	
		end

		columnIndex += 1
	end
	
	destination = getproperty(Namespace, Symbol(page.core.identifier))[Namespace.Page.properties][Namespace.Page.name]

	push!(treeView, (columnKey, valueColumns...)...)
	embed!(getElement(destination), (page.core.identifier * "TreeView") => treeView; overwrite = true)

	for (index, column) in enumerate([columnKey, valueColumns...])
		GAccessor.resizable(column, true)
		GAccessor.sort_column_id(column, index - 1)
		GAccessor.reorderable(column, index)
	end

	for (cellType, cellCallbackFlag, cellCallback, longCallback) in editables
		if longCallback
			signal_connect((widget, row, value) -> cellCallback(widget, row, value, page), cellType, cellCallbackFlag)
		else
			signal_connect((widget, row) -> cellCallback(widget, row, page), cellType, cellCallbackFlag)
		end
	end

	getElement(destination) |> showall
end

function destroyPage(page::PageType) where PageType <: AbstractPageTree
	isvalid(page) && isNamespaced(page)

	destination = getproperty(Namespace, Symbol(page.core.identifier))[Namespace.Page.properties][Namespace.Page.name]

	for child in getElement(destination)
		letgo(child; remove = true)
	end

	letgo(page.core.identifier * "ListStore") # list store is not a proper child, but remove it anyways

	return nothing
end

function addSubpage!(page::PageType, subpage::SubpageType) where {PageType <: AbstractPageTree, SubpageType <: AbstractPage}
	isvalid(page) && isvalid(subpage) && isCreateable(subpage)

	destination = getproperty(Namespace, Symbol(page.core.identifier))[Namespace.Page.properties][Namespace.Page.name]

	subpage_object = createPageFrom(subpage.core)
	page.subpages[subpage.core.element_name] = subpage
	embed!(getElement(destination), subpage.core.element_name => subpage_object; overwrite = true)

	# Try to create subpage, if there is any method available
	try
		createPage(subpage)
	catch end

	getElement(subpage) |> showall
end

function addControls(page::PageType, save_system::SaveSystem = getGlobalSaveSystem()) where PageType <: AbstractPageTree
	button = ButtonPage(page.core.identifier * "_Add_New", "Add new")

	addSubpage!(page, button)

	signal_connect(addNewEntry, getElement(button), :clicked, Nothing, (), false, (page, save_system))
end

function addNewEntry(widget, parameters)
	page, save_system = parameters

	listStore = getElement(page.core.identifier * "ListStore")

	default_entity = page.core.type()
	default_entity.core.in_save = save_system

	processTemporaryEntity!(default_entity)
	values = getproperties(default_entity; simplify = true, only_values = true)
	
	push!(listStore, (default_entity.core.id, values...))

	return nothing
end

end