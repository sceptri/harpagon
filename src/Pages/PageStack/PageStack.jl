module PageStack

using Gtk

import Base: isvalid
import ..Pages: BlankCorePage, AbstractPage, AbstractPageComposite, createPage, createPageFrom, destroyPage, isvalid, contains, CorePage, ObjectCorePage, isElementorFriendly, isnamed, isCreateable
import ...Harpagon: SaveSystem, Namespace, getElement, embed, embed!, letgo, getGlobalSaveSystem, push!, set_stack!
import ...Entities: AbstractEntity, Entity, getproperties, setproperty, propertytypes, propertynames, getid, query, findby

export AbstractPageStack, createPage, destroyPage, addSubpage!, reload!, isvalid

# Page type including GtkStack and GtkStackSwitcher
abstract type AbstractPageStack <: AbstractPageComposite end

isvalid(page::PageType; panic = true) where PageType <: AbstractPageStack = (Base.@invoke isvalid(page::AbstractPageComposite; panic = panic)) && 
	contains(page, :stack_page, AbstractPage; panic = panic) && contains(page, :switcher_page, AbstractPage; panic = panic) && 
	isElementorFriendly(page.switcher_page) && isElementorFriendly(page.stack_page)

function createPage(page::PageType) where PageType <: AbstractPageStack
	error("Not yet implemented!")
end

function destroyPage(page::PageType) where PageType <: AbstractPageStack
	error("Not yet implemented!")
end

# TODO: Rewrite TitledPage and this function to be more economic
function addSubpage!(page::PageType, subpage::SubpageType) where {PageType <: AbstractPageStack, SubpageType <: AbstractPage}
	isvalid(page) && isvalid(subpage) && isnamed(subpage) && isCreateable(subpage)

	subpage_title = subpage.name
	if contains(subpage, :title, AbstractString)
		subpage_title = subpage.title
	end

	subpage_object = createPageFrom(subpage.core)

	push!(getElement(page.stack_page), subpage_object, subpage.name, subpage_title)
	# TODO: Merge push to elementor and push to subpages to one call (and possibly even embed)
	embed(subpage.name => subpage_object; overwrite = true)
	page.subpages[subpage.core.element_name] = subpage

	# Try to create subpage, if there is any method available
	try
		createPage(subpage)
	catch end
	
	reload!(page)
end

function reload!(page::PageType) where PageType <: AbstractPageStack
	isvalid(page)
	set_stack!(getElement(page.switcher_page), getElement(page.stack_page))	
end

# Generic implementation ------------
"""
Generic implementation of AbstractPageStack
"""
Base.@kwdef mutable struct GenericPageStack{T <: CorePage, S <: AbstractPage, P <: AbstractPage} <: AbstractPageStack
	core::T
	subpages::Dict{String,Any} = Dict{String,Any}()
	stack_page::S 
	switcher_page::P
end

end