using Gtk

import Base: isvalid, contains
import ..Harpagon: getElement, Elementor, getElementor

# Abstract page concrete types ------------------------------------------------------------
"""
Example implementation of a element page
"""
Base.@kwdef mutable struct ElementPage <: AbstractPage
	core::ElementCorePage
end
suitableCore(pageType::Type{PageType}, element_name::String = "") where PageType <: ElementPage = ElementCorePage(element_name)
ElementPage(element_name::String) = ElementPage(suitableCore(ElementPage, element_name))

"""
Example implementation of a label page
"""
Base.@kwdef mutable struct LabelPage <: AbstractPage
	core::ObjectCorePage
	text::String = ""
end
suitableCore(pageType::Type{PageType}, element_name::String = "", text::String = "") where PageType <: LabelPage = ObjectCorePage(GtkLabel, (text,), element_name)
LabelPage(element_name::String, text::String = "") = LabelPage(suitableCore(LabelPage, element_name, text), text)

"""
Example implementation of a button page
"""
Base.@kwdef mutable struct ButtonPage <: AbstractPage
	core::ObjectCorePage
	text::String = ""
end
suitableCore(pageType::Type{PageType}, element_name::String = "", text::String = "") where PageType <: ButtonPage = ObjectCorePage(GtkButton, (text,), element_name)
ButtonPage(element_name::String, text::String = "") = ButtonPage(suitableCore(ButtonPage, element_name, text), text)
