import ..Harpagon: Namespace, getGlobalPreferences, pretty_print, getElement, getPagesIn

Base.@kwdef mutable struct Categorization{CorePage <: ElementorCorePage} <: AbstractPageBox
	core::CorePage
	subpages::Dict{String,Any} = Dict{String,Any}()
end
suitableCore(pageType::Type{PageType}, element_name::String) where PageType <: Categorization = ElementCorePage(element_name)
Categorization(name::String) = Categorization(; core = suitableCore(Categorization, name)) 
Categorization(name::String, orientation::Symbol) = Categorization(; core = suitableCore(Categorization, name, orientation)) 

function createPage(page::PageType) where PageType <: Categorization
	preferences = getGlobalPreferences()

	name = Namespace.Page.name
	title = Namespace.Page.title
	key = Namespace.Page.key

	categorization = Namespace.categorization[key]

	for subpage_key in getPagesIn(preferences, categorization)
		preferences_entry = preferences.save.contents[subpage_key]

		genericLabel = LabelPage("Label_" * preferences_entry[name], preferences_entry[title])
		addSubpage!(page, genericLabel)

		set_gtk_property!(getElement(genericLabel), :hexpand, true)

		genericBox = GenericPageBox(preferences_entry[name], :v)
		addSubpage!(page, genericBox)
	end
end