Base.@kwdef mutable struct Terms{T <: AbstractEntity} <: AbstractPageTree
	core::NamespacedCorePage{T}
	subpages::Dict{String, Any} = Dict{String, Any}()
end

Terms(identifier::String, type::Type{T}) where T <: AbstractEntity = Terms(; core = NamespacedCorePage(identifier, type))