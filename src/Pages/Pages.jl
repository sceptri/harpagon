# Aggregator module to include all files in Pages directory
module Pages
	import ..Harpagon: 	SaveSystem, getGlobalSaveSystem
	import ..Entities: 	AbstractEntity

	include("page.jl")
	include("common_pages.jl")

	# include all the various page structures
	include("PageTree/PageTree.jl")
	using .PageTree
	include("PageStack/PageStack.jl")
	using .PageStack
	include("PageBox/PageBox.jl")
	using .PageBox

	include("window.jl")
	include("categories.jl")
	include("tags.jl")
	include("categorization.jl")
	include("settings.jl")
	include("overview.jl")
	include("subjects.jl")
	include("terms.jl")
	include("transactions.jl")
	include("budgets.jl")
	include("budgetting.jl")

	export AbstractPage, CorePage, ObjectCorePage 
	export ElementPage, TitledPage, LabelPage, ButtonPage, GenericPageBox, GenericPageStack
	export Subjects, Transactions, Window, Categorization, Categories, Tags, Terms, Budgetting, Budgets
	export createPage, createPageFrom, destroyPage, addSubpage!, reload!, addControls, refreshData!
	export WindowUtils, PageStack, PageTree, PageBox, PageStack
end