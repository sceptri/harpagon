using Gtk

import Base: isvalid, contains
import ..Harpagon: getElement, Elementor, getElementor

# Supertype of ALL (and I mean ALL) pages ------------------------------------------------
abstract type PageBlueprint end

# Supertype for all non-core pages -> all non-core pages must include some Core page ---------------------
abstract type AbstractPage <: PageBlueprint end

"""
Composite page MUST include dictionary `subpages` field
"""
abstract type AbstractPageComposite <: AbstractPage end

function createPage(page::PageType) where PageType <: AbstractPage
	error("Page MUST implement specific page type and it's creation")
end

function destroyPage(page::PageType) where PageType <: AbstractPage
	error("Page MUST implement specific page type and it's destruction")
end

"""
Deprecated! Do NOT use

Use `page.core` instead
"""
function getcore(page::PageType) where PageType <: AbstractPage
	isvalid(page)

	return page.core
end

isvalid(page::PageType; panic = true) where PageType <: AbstractPage 			= contains(page, :core, CorePage; panic = panic)
isvalid(page::PageType; panic = true) where PageType <: AbstractPageComposite 	= (Base.@invoke isvalid(page::AbstractPage; panic = panic)) && contains(page, :subpages, Dict; panic = panic)

isnamed(page::PageType; panic = true) where PageType <: AbstractPage = contains(page, :name, AbstractString; panic = panic)

# Core Page definition and logic ----------------------------------------------------------------------

abstract type CorePage <: PageBlueprint end

abstract type ElementorCorePage <: CorePage end

isElementorFriendly(page::PageType; panic = true) where PageType <: AbstractPage = (!(typeof(page.core) <: ElementorCorePage) && panic) ? error("Core page of $page does NOT directly support Elementor") : true
getElement(core::CorePageType, elementor::Elementor = getElementor()) where CorePageType <: ElementorCorePage = getElement(core.element_name, elementor)


# --- Core page types ---
"""
Useful for querying and holding pages that take data about an entity from the save file
"""
Base.@kwdef mutable struct NamespacedCorePage{T <: AbstractEntity} <: CorePage
	identifier::String = "" # identifier in Namespace
	type::Type{T} = Entity
end

isNamespaced(page::PageType; panic = true) where PageType <: AbstractPage = (!(typeof(page.core) <: NamespacedCorePage) && panic) ? error("Core page of $page is NOT namescaped!") : true

"""
Useful for referencing gtk objects from builder or created elsewhere
"""
Base.@kwdef mutable struct ElementCorePage <: ElementorCorePage
	element_name::String = ""
end

"""
Useful page for creating new singular objects
"""
Base.@kwdef mutable struct ObjectCorePage{ObjectType <: GObject} <: ElementorCorePage
	object::Type{ObjectType}
	args::Tuple = () # empty tuple
	element_name::String = ""
end

isCreateable(page::PageType; panic = true) where PageType <: AbstractPage = (!(typeof(page.core) <: ObjectCorePage) && panic) ? error("Core page of $page does NOT support creating GTK object from itself") : true

# TODO: Add created object to Elementor
createPageFrom(page::ObjectCorePage) = page.object(page.args...)

"""
Generic page that can serve as page core
"""
Base.@kwdef mutable struct BlankCorePage <: CorePage end

"""
AbstractCorePager is Core page for holding Abstract page as a field
	
useful as there cannot be composite Core page
"""
Base.@kwdef mutable struct AbstractCorePager{PageType <: AbstractPage} <: CorePage
	abstract_page::PageType
end

# Methods for any page under PageBlueprint -------------------------------------------------------------

contains(page::AnyPageType, field::Symbol, supertype::Type = Type{Any}; panic = false) where AnyPageType <: PageBlueprint = begin
	if hasfield(typeof(page), field) && typeof(getfield(page, field)) <: supertype
		return true
	elseif panic
		error("Error: Supplied page $page does NOT have field $field of type $(supertype)! Aborting...")
	end

	return false
end

# Continuation of AbstractPage methods -------------------------------------------

suitableCore(pageType::Type{PageType}) where PageType <: AbstractPage = BlankCorePage()

getElement(page::PageType, elementor::Elementor = getElementor()) where PageType <: AbstractPage = isElementorFriendly(page) && getElement(page.core, elementor)