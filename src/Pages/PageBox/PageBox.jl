module PageBox

using Gtk

import ..Pages: ObjectCorePage, AbstractPage, AbstractPageComposite, createPage, createPageFrom, destroyPage, isvalid, isCreateable, isnamed, suitableCore, addSubpage!, isElementorFriendly
import ...Harpagon: SaveSystem, getGlobalSaveSystem, getElement, embed!, embed

export AbstractPageBox, createPage, destroyPage, TitledPage, GenericPageBox, suitableCore, addSubpage!

abstract type AbstractPageBox <: AbstractPageComposite end

function suitableCore(pageType::Type{PageType}, page_name::String, orientation::Symbol = :h) where PageType <: AbstractPageBox
	return ObjectCorePage(GtkBox, (orientation,), page_name)
end

# TODO: Maybe even remove - I am not sure it is needed
function createPage(page::PageType) where PageType <: AbstractPageBox
	error("Not yet implemented!")
end

function destroyPage(page::PageType) where PageType <: AbstractPageBox
	error("Not yet implemented!")
end

function addSubpage!(page::PageType, subpage::SubpageType) where {PageType <: AbstractPageBox, SubpageType <: AbstractPage}
	isvalid(page) && isvalid(subpage) && isCreateable(subpage) && isElementorFriendly(page)

	subpage_object = createPageFrom(subpage.core)
	page.subpages[subpage.core.element_name] = subpage
	embed!(getElement(page), subpage.core.element_name => subpage_object; overwrite = true)

	# Try to create subpage, if there is any method available
	try
		createPage(subpage)
	catch end

	getElement(subpage) |> showall
end

"""
Example implementation of a page box
"""
Base.@kwdef mutable struct GenericPageBox <: AbstractPageBox
	core::ObjectCorePage
	subpages::Dict{String,Any} = Dict{String,Any}()
end
GenericPageBox(name::String, orientation::Symbol = :h) = GenericPageBox(; core = suitableCore(GenericPageBox, name, orientation))

"""
Example implementation of a titled page
"""
Base.@kwdef mutable struct TitledPage <: AbstractPageBox
	core::ObjectCorePage
	name::String = ""
	title::String = ""
	subpages::Dict{String,Any} = Dict{String,Any}()
end

TitledPage(name::String, title::String, orientation::Symbol = :h) = TitledPage(; name = name, title = title, core = suitableCore(TitledPage, name, orientation))

end