Base.@kwdef mutable struct Transactions{T <: AbstractEntity} <: AbstractPageTree
	core::NamespacedCorePage{T}
	subpages::Dict{String, Any} = Dict{String, Any}()
end

Transactions(identifier::String, type::Type{T}) where T <: AbstractEntity = Transactions(; core = NamespacedCorePage(identifier, type))