> This is a draft of workflow of Harpagon

# Main activities
- Creating Transactions
  - Specifying subjects, categories, tags, etc.
- Managing Terms
  - Term is a period of time
  - Transactions with date inside this period are thought of as belonging to the term
  - Terms are building blocks of reports
  - Each term can have key actors, which are considered when talking about income/spending division
  - Terms has a assigned budget
- Report Generation
  - Reports are PDF/HTML files which summarize asset flow in a given term
  - It should be clear from a report which key actor owes, or is owed, which amount of money to which key actor
- Overview
  - *Sort of* interactive view of a given (default is current) term
- Blueprints
  - Blueprints should be **dynamic** templates for generation of an entity
    - They should support dynamic fill-in of expected subject/category/tag/etc.
    - Or term budget automatically estimated based on previous terms


# Modularity
I am playing around with the idea of exposed Julia code for certain parts of e.g. generation of entities, to allow for greater modularity.

But I am **not** sure about the implementation (and which parts of the code should be exposed in this way).