println("Setting dark mode...")
ENV["GTK_THEME"]="Adwaita:dark"

import Pkg;
Pkg.activate("/home/sceptri/Documents/Dev/Julia/Harpagon")

include("src/Harpagon.jl")